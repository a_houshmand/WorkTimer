class Todo {
  int _id;
  String _sbj;
  var _duration;
  var _date;
  String _desc;

  Todo(this._sbj, this._duration, this._date, this._desc);

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  String get sbj => _sbj;

  set sbj(String value) {
    _sbj = value;
  }

  get duration => _duration;

  set duration(value) {
    _duration = value;
  }

  get date => _date;

  set date(value) {
    _date = value;
  }

  String get desc => _desc;

  set desc(String value) {
    _desc = value;
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (id != null) map['id'] = _id;
    map['sbj'] = _sbj;
    map['duration'] = _duration;
    map['date'] = _date;
    map['desc'] = _desc;

    return map;
  }

  // Extract a Note object from a Map object
  Todo.fromMapObject(Map<String, dynamic> map) {
    this._id = map['id'];
    this._sbj = map['sbj'];
    this._duration = map['duration'];
    this._date = map['date'];
    this._desc = map['desc'];
  }
}
