import 'package:intl/intl.dart';

class Utils {
  static int getCurrentDate() {
    return new DateTime.now().millisecondsSinceEpoch;
  }

  static String getDuration(int timerMilis) {
    Duration duration = Duration(milliseconds: timerMilis.round());
    return [duration.inHours, duration.inMinutes, duration.inSeconds]
        .map((seg) => seg.remainder(60).toString().padLeft(2, '0'))
        .join(':');
  }

  static String readTimestamp(int timestamp) {
    if (timestamp == 0) return "--";
    var date = new DateTime.fromMillisecondsSinceEpoch(timestamp);
    var format = DateFormat('yyyy-MM-dd HH:mm');
    var time = format.format(date);
    return time.toString();
  }
}
