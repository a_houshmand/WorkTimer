import 'package:flutter/material.dart';
import 'package:list/Models/todo.dart';
import 'package:list/Utils/database_helper.dart';

import 'dart:io' show Platform;

class AddWork extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddWorkState();
  }
}

class AddWorkState extends State<AddWork> {
  DatabaseHelper helper = DatabaseHelper();

  TextEditingController titleController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;

    return WillPopScope(
        onWillPop: () {
          moveToLastScreen();
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text("Add Work"),
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  // Write some code to control things, when user press back button in AppBar
                  moveToLastScreen();
                }),
          ),
          body: Padding(
            padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
            child: ListView(
              children: <Widget>[
                // Second Element
                Padding(
                  padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                  child: TextField(
                    controller: titleController,
                    style: textStyle,
                    onChanged: (value) {
                      debugPrint('Something changed in Title Text Field');
//                      updateTitle();
                    },
                    decoration: InputDecoration(
                        labelText: 'Subject',
                        labelStyle: textStyle,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: ButtonTheme(
                          height: 45,
                          child: RaisedButton(
                            color: Theme.of(context).accentColor,
                            textColor: Theme.of(context).primaryColorDark,
                            child: Text(
                              'Add',
                              textScaleFactor: 1.5,
                            ),
                            onPressed: () {
                              setState(() {
                                _save();
                              });
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  void moveToLastScreen() {
    Navigator.pop(context, true);
  }

  // Save data to database
  void _save() async {
    try {
      print("save request");

      int result;
      if (titleController.text == "") {
        _showAlertDialog('Status', 'Please enter subject');
        print("enter subject");
        return;
      }

      if (Platform.isAndroid) {
        // Android-specific code
      } else if (Platform.isIOS) {
        // iOS-specific code
      }

      moveToLastScreen();

      Todo todo = Todo(titleController.text, 0, 0, '');
      result = await helper.insertTodo(todo);
      print(result);
      if (result != 0) {
        print("saved Successfully");
        _showAlertDialog('Status', 'Todo Saved Successfully');
      } else {
        print("saved feiled");
        _showAlertDialog('Status', 'Problem Saving Todo');
      }
    } catch (_) {
      print("error!");
    }
  }

//  void _delete() async {
//    moveToLastScreen();
//
//    if (todo.id == null) {
//      _showAlertDialog('Status', 'No Todo was deleted');
//      return;
//    }
//
//    int result = await helper.deleteTodo(todo.id);
//    if (result != 0) {
//      _showAlertDialog('Status', 'Todo Deleted Successfully');
//    } else {
//      _showAlertDialog('Status', 'Error Occured while Deleting Todo');
//    }
//  }

  void _showAlertDialog(String title, String message) {
    AlertDialog alertDialog = AlertDialog(
      title: Text(title),
      content: Text(message),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }
}
