import 'dart:async';

import 'package:flutter/material.dart';
import 'package:list/Models/todo.dart';
import 'package:list/Utils/database_helper.dart';
import 'package:list/Utils/utils.dart';
import 'package:sqflite/sqflite.dart';

class ElapsedTime {
  final int hundreds;
  final int seconds;
  final int minutes;

  ElapsedTime({
    this.hundreds,
    this.seconds,
    this.minutes,
  });
}

class Dependencies {
  final List<ValueChanged<ElapsedTime>> timerListeners =
      <ValueChanged<ElapsedTime>>[];
  final TextStyle textStyle =
      const TextStyle(fontSize: 50, fontFamily: "Bebas Neue");
  final Stopwatch stopwatch = new Stopwatch();
}

class TimerPage extends StatefulWidget {
  final String sbj;

  const TimerPage({Key key, this.sbj}) : super(key: key);

  @override
  _TimerPageState createState() => _TimerPageState(sbj);
}

class _TimerPageState extends State<TimerPage> {
  final String sbj;
  final Dependencies dependencies = new Dependencies();
  TextEditingController _textFieldController = TextEditingController();

  final scaffoldKey = GlobalKey<ScaffoldState>();

  DatabaseHelper databaseHelper = DatabaseHelper();
  List<Todo> todoList;

  _TimerPageState(this.sbj);

  @override
  Widget build(BuildContext context) {
    if (todoList == null) {
      todoList = List<Todo>();
      updateListView();
    }

    return WillPopScope(
      onWillPop: () {
        moveToLastScreen();
      },
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text(sbj),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                // Write some code to control things, when user press back button in AppBar
                moveToLastScreen();
              }),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: TimerText(dependencies: dependencies),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                buildFloatingButton(
                    dependencies.stopwatch.isRunning ? "Lap" : "Reset",
                    leftButtonPressed),
                buildFloatingButton(
                    dependencies.stopwatch.isRunning ? "Stop" : "Start",
                    rightButtonPressed),
              ],
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: getTimesListView(),
            ))
          ],
        ),
      ),
    );
  }

  void moveToLastScreen() {
    if (!dependencies.stopwatch.isRunning)
      Navigator.pop(context, true);
    else {
      final snackBar = SnackBar(content: Text('Yay! A SnackBar!'));
      Scaffold.of(context).showSnackBar(snackBar);
      scaffoldKey.currentState.showSnackBar(snackBar);

    }
  }

  void updateListView() {
    final Future<Database> dbFuture = databaseHelper.initializeDatabase();
    dbFuture.then((database) {
      Future<List<Todo>> todoListFuture = databaseHelper.getTimesList(sbj);
      todoListFuture.then((todoList) {
        setState(() {
          this.todoList = todoList;
        });
      });
    });
  }

  void leftButtonPressed() {
    setState(() {
      if (dependencies.stopwatch.isRunning) {
        dependencies.stopwatch.stop();
        _displayDialog(context, dependencies.stopwatch.elapsedMilliseconds);
      } else
        dependencies.stopwatch.reset();
    });
  }

  void rightButtonPressed() {
    setState(() {
      if (dependencies.stopwatch.isRunning)
        dependencies.stopwatch.stop();
      else
        dependencies.stopwatch.start();
    });
  }

  Widget buildFloatingButton(String text, VoidCallback callback) {
    TextStyle roundTextStyle = TextStyle(fontSize: 14.0, color: Colors.black);
    return MaterialButton(
        padding: EdgeInsets.all(19),
        shape: CircleBorder(),
        color: Theme.of(context).accentColor,
        textColor: Theme.of(context).primaryColorDark,
        child: Text(text, style: roundTextStyle),
        onPressed: callback);
  }

  ListView getTimesListView() {
    return ListView.builder(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: todoList.length,
      itemBuilder: (BuildContext context, int position) {
        return Padding(
            padding:
                const EdgeInsets.only(left: 4, right: 4, top: 5, bottom: 10),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(todoList[position].desc),
                    Text(Utils.getDuration(todoList[position].duration)),
                    Text(Utils.readTimestamp(todoList[position].date)),
                  ],
                ),
                Divider(
                  color: Colors.grey,
                ),
              ],
            ));
      },
    );
  }

  _displayDialog(BuildContext context, int duration) async {
    AlertDialog alertDialog = AlertDialog(
      title: Text("New Session"),
      content: TextField(
        controller: _textFieldController,
        decoration: InputDecoration(hintText: "Session name"),
      ),
      actions: <Widget>[
        new FlatButton(
          child: new Text('SUBMIT'),
          onPressed: () async {
            Todo todo = Todo(sbj, duration, Utils.getCurrentDate(),
                _textFieldController.text);
            int result = await databaseHelper.insertTodo(todo);
            dependencies.stopwatch.reset();
            updateListView();
            Navigator.of(context).pop();
          },
        )
      ],
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }
}

class TimerText extends StatefulWidget {
  TimerText({this.dependencies});

  final Dependencies dependencies;

  TimerTextState createState() =>
      new TimerTextState(dependencies: dependencies);
}

class TimerTextState extends State<TimerText> {
  TimerTextState({this.dependencies});

  final Dependencies dependencies;
  Timer timer;
  int milliseconds;

  @override
  void initState() {
    timer = new Timer.periodic(new Duration(milliseconds: 30), callback);
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    timer = null;
    super.dispose();
  }

  void callback(Timer timer) {
    if (milliseconds != dependencies.stopwatch.elapsedMilliseconds) {
      milliseconds = dependencies.stopwatch.elapsedMilliseconds;
      final int hundreds = (milliseconds / 10).truncate();
      final int seconds = (hundreds / 100).truncate();
      final int minutes = (seconds / 60).truncate();
      final ElapsedTime elapsedTime = new ElapsedTime(
        hundreds: hundreds,
        seconds: seconds,
        minutes: minutes,
      );
      for (final listener in dependencies.timerListeners) {
        listener(elapsedTime);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new RepaintBoundary(
          child: new SizedBox(
            height: 72.0,
            child: new MinutesAndSeconds(dependencies: dependencies),
          ),
        ),
        new RepaintBoundary(
          child: new SizedBox(
            height: 72.0,
            child: new Hundreds(dependencies: dependencies),
          ),
        ),
      ],
    );
  }
}

class MinutesAndSeconds extends StatefulWidget {
  MinutesAndSeconds({this.dependencies});

  final Dependencies dependencies;

  MinutesAndSecondsState createState() =>
      new MinutesAndSecondsState(dependencies: dependencies);
}

class MinutesAndSecondsState extends State<MinutesAndSeconds> {
  MinutesAndSecondsState({this.dependencies});

  final Dependencies dependencies;

  int minutes = 0;
  int seconds = 0;

  @override
  void initState() {
    dependencies.timerListeners.add(onTick);
    super.initState();
  }

  void onTick(ElapsedTime elapsed) {
    if (elapsed.minutes != minutes || elapsed.seconds != seconds) {
      setState(() {
        minutes = elapsed.minutes;
        seconds = elapsed.seconds;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    return new Text('$minutesStr:$secondsStr.', style: dependencies.textStyle);
  }
}

class Hundreds extends StatefulWidget {
  Hundreds({this.dependencies});

  final Dependencies dependencies;

  HundredsState createState() => new HundredsState(dependencies: dependencies);
}

class HundredsState extends State<Hundreds> {
  HundredsState({this.dependencies});

  final Dependencies dependencies;

  int hundreds = 0;

  @override
  void initState() {
    dependencies.timerListeners.add(onTick);
    super.initState();
  }

  void onTick(ElapsedTime elapsed) {
    if (elapsed.hundreds != hundreds) {
      setState(() {
        hundreds = elapsed.hundreds;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String hundredsStr = (hundreds % 100).toString().padLeft(2, '0');
    return new Text(hundredsStr, style: dependencies.textStyle);
  }
}
