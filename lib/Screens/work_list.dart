import 'dart:async';

import 'package:flutter/material.dart';
import 'package:list/Models/todo.dart';
import 'package:list/Screens/add_work.dart';
import 'package:list/Screens/timer_page.dart';
import 'package:list/Utils/database_helper.dart';
import 'package:list/Utils/utils.dart';
import 'package:sqflite/sqflite.dart';

class WorkList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return WorkListState();
  }
}

class WorkListState extends State<WorkList> {
  DatabaseHelper databaseHelper = DatabaseHelper();
  List<Todo> todoList;
  int count = 0;

  @override
  Widget build(BuildContext context) {
    if (todoList == null) {
      todoList = List<Todo>();
      updateListView();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Work timer'),
      ),
      body: getTodoListView(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          navigateToDetail();
        },
        tooltip: 'Add',
        child: Icon(Icons.add),
      ),
    );
  }

  ListView getTodoListView() {
    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context, int position) {
        return Padding(
          padding: const EdgeInsets.only(left: 4, right: 4),
          child: Column(
            children: <Widget>[
              ListTile(
//            leading: CircleAvatar(
//              backgroundColor: Colors.amber,
//              child: Text(this.todoList[position].sbj,
//                  style: TextStyle(fontWeight: FontWeight.bold)),
//            ),
                title: Text(todoList[position].sbj,
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 20,
                        color: Colors.white)),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Text(
                    "Last: ${Utils.readTimestamp(todoList[position].date)}",
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      Utils.getDuration(todoList[position].duration),
                      style: TextStyle(color: Colors.grey),
                    )
//                GestureDetector(
//                  child: Icon(
//                    Icons.delete,
//                    color: Colors.red,
//                  ),
//                  onTap: () {
//                    _delete(context, todoList[position]);
//                  },
//                ),
                  ],
                ),
                onTap: () {
                  navigateToTimerPage(position);
                },
              ),
              Divider(
                color: Colors.grey,
              )
            ],
          ),
        );
      },
    );
  }

  void _delete(BuildContext context, Todo todo) async {
    int result = await databaseHelper.deleteTodo(todo.id);
    if (result != 0) {
      _showSnackBar(context, 'Todo Deleted Successfully');
      updateListView();
    }
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void navigateToDetail() async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return AddWork();
    }));

    if (result == true) updateListView();
  }

  void navigateToTimerPage(int index) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return TimerPage(
        sbj: todoList[index].sbj,
      );
    }));

    if (result == true) updateListView();
  }

  void updateListView() {
    final Future<Database> dbFuture = databaseHelper.initializeDatabase();
    dbFuture.then((database) {
      Future<List<Todo>> todoListFuture = databaseHelper.getTodoList();
      todoListFuture.then((todoList) {
        setState(() {
          this.todoList = todoList;
          this.count = todoList.length;
        });
      });
    });
  }
}
