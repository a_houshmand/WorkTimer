import 'package:flutter/material.dart';

import 'Screens/timer_page.dart';
import 'Screens/work_list.dart';

void main() {
  runApp(TodoApp());
}

class TodoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TodoList',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.dark,
      ),
      home: WorkList(),
    );
  }
}

//class MyHomePage extends StatelessWidget {
//  MyHomePage({Key key}) : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      appBar: new AppBar(
//        title: new Text("Stopwatch"),
//      ),
//      body: new Container(
//          child: new TimerPage()
//      ),
//    );
//  }
//}

